//Document utiliser pour valider les comptes

const validateId = (id) => {
    return typeof id === 'number' &&
    !!id;
}

const validateNom= (nom) => {
    return typeof nom === 'string' &&
    !!nom && 
    nom.length > 0 && 
    nom.length <= 50;
}

const validatePrenom= (prenom) => {
    return typeof prenom === 'string' &&
    !!prenom && 
    prenom.length > 0 && 
    prenom.length <= 50;
}

const validateAdresse = (adresse) => {
    return typeof adresse === 'string' &&
    !!adresse &&
    adresse.length > 0 &&
    adresse.length <= 100;
}

const validateCourriel = (courriel) => {
    return typeof courriel === 'string' &&
    !!courriel &&
    courriel.length > 0 &&
    courriel.length <= 50;
}

const validatePassword = (password) => {
    return typeof password === 'string' &&
    !!password &&
    password.length > 0 &&
    password.length <= 100;
}



exports.validerCompte = (compte) => {
    return validateId(compte.id_compte) && 
        validateNom(compte.nom) &&
        validatePrenom(compte.prenom) &&
        validateCourriel(compte.courriel) &&
        validateAdresse(compte.adresse) &&
        validatePassword(compte.password) &&
        validateId(compte.id_type_compte);
}

exports.validerId = validateId;
