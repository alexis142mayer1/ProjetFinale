//Document utiliser pour valider les commandes

const validateId = (id) => {
    return typeof id === 'number' &&
    !!id;
}

const validateDate = (date) => {
    return typeof date === 'string' &&
    !!date && 
    date.length === 10;
}

const validatePrix = (prix) => {
    return typeof prix === 'float' &&
    !!prix &&
    prix > 0;
}

exports.validerCommande = (commande) => {
    return validateId(commande.id_commande) && 
        validateDate(commande.date_commander) &&
        validatePrix(commande.prix_total) &&
        validateId(commande.id_compte) &&
        validateId(commande.id_statut) &&
        validateId(commande.id_employer);
}

exports.validerId = validateId;
