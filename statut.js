const connexionPromise = require('./connexion');
//Document utiliser pour acceder a la base de donnees(table statut)

exports.getIdStatut = async (dataNom) => {
    let connection = await connexionPromise;
    let results = await connection.query(
        'SELECT id_statut FROM statut WHERE nom = (?);',
        [dataNom]
    );
    return results;
}

exports.getNomStatut = async (dataId) => {
    let connection = await connexionPromise;
    let results = await connection.query(
        'SELECT nom FROM statut WHERE id_statut = (?);',
        [dataId]
    );
    return results;
}

exports.getAllStatut = async () => {
    let connection = await connexionPromise;
    let results = await connection.query(
        'SELECT nom FROM statut;'
    );
    return results;
}