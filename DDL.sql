-- Créer la base de données
DROP DATABASE IF EXISTS starbucks;
CREATE DATABASE starbucks 
CHARACTER SET utf8mb4 
COLLATE utf8mb4_unicode_ci;

-- Utiliser la base de données creer
USE starbucks;

-- Créer la table compte
DROP TABLE IF EXISTS compte;
CREATE TABLE compte(
    id_compte INT AUTO_INCREMENT,
    nom VARCHAR(50) NOT NULL,
    prenom VARCHAR(50) NOT NULL,
    courriel VARCHAR(50) NOT NULL,
    adresse VARCHAR(100) NOT NULL,
    password VARCHAR(100) NOT NULL,
    id_type_compte INT,
    CONSTRAINT PRIMARY KEY(id_compte)
);

-- Créer la table commande
DROP TABLE IF EXISTS commande;
CREATE TABLE commande(
    id_commande INT AUTO_INCREMENT,
    date_commander DATE NOT NULL,
    prix_total FLOAT NOT NULL,
    id_compte INT,
    id_employer INT,
    id_statut INT,
    CONSTRAINT PRIMARY KEY(id_commande)
);

-- Créer la table produit
DROP TABLE IF EXISTS produit;
CREATE TABLE produit(
    id_produit INT AUTO_INCREMENT,
    nom VARCHAR(50) NOT NULL,
    prix FLOAT NOT NULL,
    image VARCHAR(50) NOT NULL,
    CONSTRAINT PRIMARY KEY(id_produit)
);

-- Créer la table statut
DROP TABLE IF EXISTS statut;
CREATE TABLE statut(
    id_statut INT AUTO_INCREMENT,
    nom VARCHAR(50) NOT NULL,
    CONSTRAINT PRIMARY KEY(id_statut)
);

-- Créer la table type_compte
DROP TABLE IF EXISTS type_compte;
CREATE TABLE type_compte(
    id_type_compte INT AUTO_INCREMENT,
    nom VARCHAR(50) NOT NULL,
    CONSTRAINT PRIMARY KEY(id_type_compte)
);

-- Créer la table cours_programme
DROP TABLE IF EXISTS commande_produit;
CREATE TABLE commande_produit(
    id_commande INT,
    id_produit INT,
    quantite INT NOT NULL,
    CONSTRAINT PRIMARY KEY(id_commande, id_produit)
);


-- Ajouter les contraintes pour la clee etrangaire id_type_compte dans la table compte
ALTER TABLE compte
    ADD CONSTRAINT fk_compte_type_compte
    FOREIGN KEY(id_type_compte)
    REFERENCES type_compte(id_type_compte)
    ON DELETE RESTRICT
    ON UPDATE CASCADE;
    
-- Ajouter les contraintes pour la clee etrangaire id_compte dans la table commande
ALTER TABLE commande
    ADD CONSTRAINT fk_commande_compte
    FOREIGN KEY(id_compte)
    REFERENCES compte(id_compte)
    ON DELETE RESTRICT
    ON UPDATE CASCADE;

-- Ajouter les contraintes pour la clee etrangaire id_statut dans la table commande
ALTER TABLE commande
    ADD CONSTRAINT fk_commande_statut
    FOREIGN KEY(id_statut)
    REFERENCES statut(id_statut)
    ON DELETE RESTRICT
    ON UPDATE CASCADE;

-- Ajouter les contraintes pour la clee etrangaire id_commande dans la table commande_produit
ALTER TABLE commande_produit
    ADD CONSTRAINT fk_commande_produit_commande
    FOREIGN KEY(id_commande)
    REFERENCES commande(id_commande)
    ON DELETE RESTRICT
    ON UPDATE CASCADE;

-- Ajouter les contraintes pour la clee etrangaire id_produit dans la table commande_produit
ALTER TABLE commande_produit
    ADD CONSTRAINT fk_commande_produit_produit
    FOREIGN KEY(id_produit)
    REFERENCES produit(id_produit)
    ON DELETE RESTRICT
    ON UPDATE CASCADE;
