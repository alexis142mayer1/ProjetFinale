const express = require('express');
const helmet = require('helmet');
const compression = require('compression');
const bodyParser = require('body-parser');
const serveStatic = require('serve-static');
const cors = require('cors');
const sse = require('./middleware-sse');
const session = require('express-session');
const MemoryStore = require('memorystore')(session);

const produits = require('./produit');
const logins = require('./login');
const statuts = require('./statut');
const inscriptions = require('./inscription');
const commandes = require('./commande');
const commandeProduits = require('./commandeProduit');
const typeComptes = require('./typeCompte');

const validerCommandes = require('./validationCommande');
const validerComptes = require('./validationCompte');
const validerProduits = require('./validationProduit');
const validerStatuts = require('./validationStatut');
const validerTypeComptes = require('./validationTypeCompte');

const PORT = process.env.PORT;
let app = express();

let helmetOptions = null;
if(app.settings.env === 'development'){
    helmetOptions = require('./developement-csp');
}

app.use(helmet(helmetOptions));
app.use(compression());
app.use(cors());
app.use(bodyParser.json({ strict: false }));
app.use(serveStatic('./public'));
app.use(sse());
app.use(session({
    cookie: { maxAge: 3600000 },
    name: process.env.npm_package_name,
    store: new MemoryStore({ checkPeriod: 3600000 }),
    resave: false,
    secret: '315 301 481 333 290'
}));


//Route html

//Route utilisé pour retourner tout les produits
app.get('/produit', async (request, response) => {
    const data = await produits.getAll();
    response.status(200).json(data);
});

//Route utilisé pour retourner le panier
app.get('/panier', async (request, response) => {
    if(!request.session.listePanier){
        request.session.listePanier = [];
    }
    response.status(200).json(request.session.listePanier);
});

//Route utilisé pour ajouter des produits au panier
app.post('/panier', async (request, response) => {
    if(!request.session.listePanier){
        request.session.listePanier = [];
    }
    else{
        for(let i = 0; i < request.session.listePanier.length; i ++){
            if (request.session.listePanier[i].id == request.body.id){
                request.session.listePanier[i].quantite = parseInt(request.session.listePanier[i].quantite) + parseInt(request.body.quantite);
                response.sendStatus(200);
            }
        }
    }
        
    request.session.listePanier.push(request.body);
    response.sendStatus(200);
});

//Route utilisé pour effacer des produits du panier
app.delete('/panier', async (request, response) => {
    if(!request.session.listePanier){
        request.session.listePanier = [];
    }
    else{
        for(let i = 0; i < request.session.listePanier.length; i ++){
            if (request.session.listePanier[i].id == request.body.id){
                request.session.listePanier.splice(i, 1);
            }
        }
    }

    response.sendStatus(200);
});

//Route utilisé pour retourner le compte de la session
app.get('/compte', async (request, response) => {
    if(!request.session.compte){
        request.session.compte = "";
        response.sendStatus(401);
    }
    else{
        response.status(200).json(request.session.compte);
    }
});

//Route utilisé mettre a jour l'information de compte
app.post('/updateCompte', async (request, response) => {
    await inscriptions.updateCompte(request.body.nom, request.body.prenom, request.body.adresse, request.body.courriel, request.body.id);
    request.session.compte.nom = request.body.nom;
    request.session.compte.prenom = request.body.prenom;
    request.session.compte.courriel = request.body.courriel;
    request.session.compte.adresse = request.body.adresse;
    
    response.sendStatus(200);
});

//Route utilisé retourne code html si client est identifier par un compte dans la session
app.get('/identifier', async (request, response) => {
    if(!request.session.compte){
        response.sendStatus(401);
    }
    response.sendStatus(200);
});

//Route utilisé efface le compte de la session
app.get('/logout', async (request, response) => {
    request.session.compte = "";
    response.sendStatus(200);
});

//Route utilisé pour retourner le id_statut du nom fourni
app.post('/statut', async (request, response) => {
    let data = await statuts.getIdStatut(request.body.nom);
    response.status(200).json(data);
});

//Route utilisé pour retourner tout les statuts
app.get('/statut', async (request, response) => {
    let data = await statuts.getAllStatut();
    response.status(200).json(data);
});

//Route utilisé pour retourner le nom du id_statut fourni
app.post('/statutNom', async (request, response) => {
    let data = await statuts.getNomStatut(request.body.id);
    response.status(200).json(data);
});

//Route utilisé pour retourner le id_type_compte du nom fourni
app.post('/typeCompte', async (request, response) => {
    let data = await typeComptes.getIdTypeCompte(request.body.nom);
    response.status(200).json(data);
});

//Route utilisé pour retourner les commandes avec un statut qui n'est pas 'Terminer'
app.post('/commandeActive', async (request, response) => {
    let data = await commandes.getCommandeActive(request.body.id, request.body.id01, request.body.id02, request.body.id03);
    response.status(200).json(data);
});

//Route utilisé pour mettre a jour le statut d'une commande
app.post('/commandeStatut', async (request, response) => {
    let data = await commandes.updateCommandeStatut(request.body.id, request.body.idStatut);
    response.status(200).json(data);
});

//Route utilisé pour retourner tout les commandes d'un client
app.post('/commandeAll', async (request, response) => {
    let data = await commandes.getAllCommande(request.body.id);
    response.status(200).json(data);
});

//Route utilisé pour ajouter un id_employer a une commande
app.post('/acquerirCommandeEmployer', async (request, response) => {
    await commandes.acquerirCommande(request.body.idEmployer, request.body.idCommande);
    response.sendStatus(200);
});

//Route utilisé pour retourner tout les commandes
app.get('/commandeAllEmployer', async (request, response) => {
    let data = await commandes.getAllCommandeEmployer();
    response.status(200).json(data);
});

//Route utilisé pour retourner tout les commandes qui a ete acqueris par cette employer
app.post('/commandeEmployer', async (request, response) => {
    let data = await commandes.getCommandeEmployer(request.body.id);
    response.status(200).json(data);
});

//Route utilisé pour ajouter dans la table relationnel commande_produit
app.post('/commandeProduit', async (request, response) => {
    await commandeProduits.addCommandeProduit(request.body.idCommande, request.body.idProduit, request.body.quantite);
    response.sendStatus(200);
});

//Route utilisé pour ajouter des commandes
app.post('/commande', async (request, response) => {
    let data = await commandes.addCommande(request.body.id, request.body.dateCommander, request.body.total, request.body.idStatut);
    response.status(200).json(data);
});

//Route utilisé pour login avec un compte
app.post('/login', async (request, response) => {

    let dataCompte = await logins.getCompte(request.body.username, request.body.password);

    if(dataCompte.length == 0){
        response.sendStatus(401);
    }

    request.session.compte = dataCompte[0];
    response.sendStatus(200);
});

//Route utilisé pour ajouter un compte et login
app.post('/inscription', async (request, response) => {
    await inscriptions.addCompte(request.body.nom, request.body.prenom, request.body.adresse, request.body.courriel, request.body.password, request.body.IdTypeCompte);
    let dataCompte = await logins.getCompte(request.body.courriel, request.body.password);

    if(dataCompte.length == 0){
        response.sendStatus(401);
    }

    request.session.compte = dataCompte[0];
    response.sendStatus(200);
});



app.use(function (request, response) {
    response.status(404).send(request.originalUrl + ' not found.');
});

app.listen(PORT);
