//Document utiliser pour valider les type_comptes

const validateName = (nom) => {
    return typeof nom === 'string' &&
    !!nom && 
    nom.length > 0 && 
    nom.length <= 50;
}

exports.validerNom = validateName;
