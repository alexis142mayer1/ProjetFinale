//Document utiliser pour valider les produits

const validateId = (id) => {
    return typeof id === 'number' &&
    !!id;
}

const validateTitre = (titre) => {
    return typeof titre === 'string' &&
    !!titre && 
    titre.length > 0 && 
    titre.length <= 50;
}

const validatePrix = (prix) => {
    return typeof prix === 'float' &&
    !!prix &&
    prix > 0;
}

const validateImage = (image) => {
    return typeof image === 'string' &&
    !!image &&
    image.length > 0 && 
    image.length <= 50;
}

const validateQuantite = (quantite) => {
    return typeof quantite === 'number' &&
    !!quantite &&
    quantite > 0;
}

exports.validerProduit = (produit) => {
    return validateId(produit.id) && 
        validateTitre(produit.titre) &&
        validatePrix(produit.prix) &&
        validateImage(produit.image) &&
        validateQuantite(produit.quantite);
}

exports.validerId = validateId;
