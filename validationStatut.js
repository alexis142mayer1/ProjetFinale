//Document utiliser pour valider les statuts

const validateName = (nom) => {
    return typeof nom === 'string' &&
    !!nom && 
    nom.length > 0 && 
    nom.length <= 50;
}

exports.validerNom = validateName;
