const connexionPromise = require('./connexion');
//Document utiliser pour acceder a la base de donnees(table compte)

exports.addCompte = async ( dataNom, dataPrenom, dataAdresse, dataCourriel, dataPassword, dataIdTypeCompte) => 
{
    let connection = await connexionPromise;
    await connection.query(
        'INSERT INTO compte(nom, prenom, password, courriel, adresse, id_type_compte) VALUE (?,?,?,?,?,?);',
        [dataNom, dataPrenom, dataPassword, dataCourriel, dataAdresse, dataIdTypeCompte]
    );
}

exports.updateCompte = async ( dataNom, dataPrenom, dataAdresse, dataCourriel, dataId) => 
{
    let connection = await connexionPromise;
    await connection.query(
        'UPDATE compte SET nom = (?), prenom = (?), courriel = (?), adresse = (?) where id_compte = (?);',
        [dataNom, dataPrenom, dataCourriel, dataAdresse, dataId]
    );
}