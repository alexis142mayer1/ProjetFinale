const connexionPromise = require('./connexion');
//Document utiliser pour acceder a la base de donnees(table commande_produit)

exports.addCommandeProduit = async (dataIdCommande, dataIdProduit, dataQuantite) => 
{
    let connection = await connexionPromise;
    await connection.query(
        'INSERT INTO commande_produit(id_commande, id_produit, quantite) VALUE (?,?,?);',
        [dataIdCommande, dataIdProduit, dataQuantite]
    );
}