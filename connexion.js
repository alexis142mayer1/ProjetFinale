let mysql = require('promise-mysql');

let connectionPromise = mysql.createPool({
    connectionLimit: 10,
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'starbucks',
    multipleStatements: true
});

module.exports = connectionPromise;