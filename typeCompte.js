const connexionPromise = require('./connexion');
//Document utiliser pour acceder a la base de donnees(table compte)

exports.getIdTypeCompte = async (dataNom) => {
    let connection = await connexionPromise;
    let results = await connection.query(
        'SELECT id_type_compte FROM type_compte WHERE nom = (?);',
        [dataNom]
    );
    return results;
}