INSERT INTO compte(nom, prenom, courriel, adresse, password, id_type_compte)
VALUE("Mayer", "Alexis", "test1@exemple.com", "123 main st.", "12345", 1);

INSERT INTO compte(nom, prenom, courriel, adresse, password, id_type_compte)
VALUE("Mayer", "Patrick", "test2@exemple.com", "234 main st.", "12345", 1);

INSERT INTO compte(nom, prenom, courriel, adresse, password, id_type_compte)
VALUE("Mayer", "Patrick", "test2@exemple.com", "234 main st.", "12345", 2);

INSERT INTO type_compte(nom)
VALUE("Client");

INSERT INTO type_compte(nom)
VALUE("Employer");

INSERT INTO statut(nom)
VALUE("En traitement");

INSERT INTO statut(nom)
VALUE("En cuisine");

INSERT INTO statut(nom)
VALUE("En livraison");

INSERT INTO statut(nom)
VALUE("Terminer");

INSERT INTO produit(nom, prix, image)
VALUE("Matcha drink", 2.99, "MatchaDrink.jpg");

INSERT INTO produit(nom, prix, image)
VALUE("Mocha frappuccino", 4.99, "MochaFrappuccino.jpg");

INSERT INTO produit(nom, prix, image)
VALUE("Vanilla sweet coldbrew", 3.99, "VanillaSweetColdbrew.jpg");

INSERT INTO produit(nom, prix, image)
VALUE("Limonade hibiscus", 5.99, "LimonadeHibiscus.jpg");

INSERT INTO produit(nom, prix, image)
VALUE("Chocolat chaud", 1.99, "ChocolatChaud.jpg");

INSERT INTO produit(nom, prix, image)
VALUE("Chai latte", 3.99, "ChaiLatte.jpg");