const connexionPromise = require('./connexion');
//Document utiliser pour acceder a la base de donnees(table produit)

exports.getAll = async () => {
    let connection = await connexionPromise;
    let results = await connection.query(
        'SELECT * FROM produit'
    );
    return results;
}