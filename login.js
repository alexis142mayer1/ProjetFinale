const connexionPromise = require('./connexion');
//Document utiliser pour acceder a la base de donnees(table compte)

exports.getCompte = async (dataUsername, dataPassword) => 
{
    let connection = await connexionPromise;
    let results = await connection.query(
        'SELECT * FROM compte WHERE courriel = (?) AND password = (?);',
        [dataUsername, dataPassword]
    );
    return results;
}