const connexionPromise = require('./connexion');
//Document utiliser pour acceder a la base de donnees(table commande)

exports.addCommande = async (dataId, dataDate, dataTotal, dataIdStatut) => {
    let connection = await connexionPromise;
    let results = await connection.query(
        'INSERT INTO commande(date_commander, prix_total, id_compte, id_statut) VALUE (?,?,?,?); SELECT LAST_INSERT_ID();', 
        [dataDate, dataTotal, dataId, dataIdStatut]
    );

    return results[1][0]["LAST_INSERT_ID()"];
}

exports.getAllCommande = async (dataId) => {
    let connection = await connexionPromise;
    let results = await connection.query(
        'SELECT * FROM commande WHERE id_compte = (?);',
        [dataId]
    );
    return results;
}

exports.getAllCommandeEmployer = async () => {
    let connection = await connexionPromise;
    let results = await connection.query(
        'SELECT * FROM commande;'
    );
    return results;
}

exports.getCommandeActive = async (dataId, dataIdStatut01, dataIdStatut02, dataIdStatut03) => {
    let connection = await connexionPromise;
    let results = await connection.query(
        'SELECT * FROM commande WHERE id_compte = (?) AND id_statut IN (?,?,?);',
        [dataId, dataIdStatut01, dataIdStatut02, dataIdStatut03]
    );
    return results;
}

exports.updateCommandeStatut = async (dataId, dataIdStatut) => {
    let connection = await connexionPromise;
    let results = await connection.query(
        'UPDATE commande SET id_statut = (?) where id_commande = (?);',
        [dataIdStatut, dataId]
    );
    return results;
}

exports.getCommandeEmployer = async (dataId) => {
    let connection = await connexionPromise;
    let results = await connection.query(
        'SELECT * FROM commande WHERE id_employer = (?);',
        [dataId]
    );
    return results;
}

exports.acquerirCommande = async (dataIdEmployer, dataIdCommande) => {
    let connection = await connexionPromise;
    let results = await connection.query(
        'UPDATE commande SET id_employer = (?) where id_commande = (?);',
        [dataIdEmployer, dataIdCommande]
    );
    return results;
}