(() => {
    let tablePanier = document.getElementById('tablePanier');
    let templateRow = document.getElementById('template-row');
    let txtSousTotal = document.getElementById("txtSousTotal");
    let txtTotal = document.getElementById("txtTotal");
    let txtLivraison = document.getElementById("txtLivraisonPrix");
    let txtTitre = document.getElementById("txtPanierTitre");
    let tBody = document.getElementById("tBodyPanier");
    let btnProceder = document.getElementById("btnProceder");

    let listePanier = [];
    let Totaux = 0;

    //Fonction pour ajouter une table row au client
    const ajouterProduitRow = (id, titre, prix, image, quantite) => {
        let tr = templateRow.content.firstElementChild.cloneNode(true);

        tr.querySelector('.txtTitre').innerText = titre;
        tr.querySelector('.txtPrix').innerText = prix + "$";
        tr.querySelector('.imgTable').src = "./img/" + image;
        tr.querySelector('.txtQuantite').innerText = quantite;
        tr.querySelector('.btnRetirer').addEventListener('click', supprimerPanierServeur);

        tr.dataset.id = id;
        Totaux += parseInt(quantite) * parseFloat(prix);

        tBody.append(tr);
    }

    //Fonction pour chercher les produits du panier
    const getPanierServeur = async () => {
        Totaux = 0;
        listePanier = [];
        let response = await fetch('/panier');

        if(response.ok){
            listePanier = await response.json();

            for(let produit of listePanier){
                ajouterProduitRow(produit.id, produit.titre, produit.prix, produit.image, produit.quantite);
            }
            ActualiserPanier();
        }
    }

    //Fonction pour effacer des produits du panier
    const supprimerPanierServeur = async (event) => {
        let row = event.target.parentNode.parentNode.dataset.id;

        fetch('/panier', {
            method: 'DELETE',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
                id: row
            })
        });
        
        viderPanierLocal();
        getPanierServeur();
    }

    //Fonction pour vider le tableau du client
    const viderPanierLocal = () => {
        tBody.innerHTML = "";
    }

    //Fonction pour calculer et mettre a jour le panier
    const ActualiserPanier = () => {

        txtLivraison.innerText = 3.99;

        if(listePanier.length != 0){
            txtSousTotal.innerText = Totaux.toFixed(2);
            txtTotal.innerText = (parseFloat(txtLivraison.innerText) + parseFloat(Totaux)).toFixed(2);

            txtTitre.innerText = "Panier";
            tablePanier.style.visibility = "visible";
            txtSousTotal.innerText += " $";
            txtTotal.innerText += " $";

            if(txtLivraison.innerText.indexOf('$') == -1){
                txtLivraison.innerText += " $";
            }
        }
        else{
            txtTitre.innerText = "*  Panier vide  *";
            tablePanier.style.visibility = "hidden";
            txtSousTotal.innerText = "0 $";
            txtTotal.innerText = "0 $";
            txtLivraison.innerText = "0 $";
        }
    }

    //Fonction avant de proceder a la revision de la commande
    const procederPanier = async () => {

        if(listePanier.length != 0){
            let response = await fetch('/identifier');

            if(response.status == 200){
                window.location.replace("./revision.html");
             }
            else{   
                alert("Vous devez être identifié avant de pouvoir procéder une commande.");
            }
        }
        else{
            alert("Votre panier est vide.\nPour procéder, vous devez ajouter des produits.");
        }
    }

    viderPanierLocal();
    getPanierServeur();
    btnProceder.addEventListener('click', procederPanier);
})();