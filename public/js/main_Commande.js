(() => {
    let templateRow = document.getElementById('template-row');
    let tableCommande = document.getElementById("tableCommande");
    let txtTitre = document.getElementById("txtTitre");
    let btnLogout = document.getElementById("btnLogout");
    
    let idCompte = 0;
    let listeCommande = [];
    
    //Fonction pour ajouter row dans la table de commande
    const ajouterCommandeRow = (id, date, prix, statut) => {
        let tr = templateRow.content.firstElementChild.cloneNode(true);
        tr.querySelector('.txtDate').innerText = date.substring(0, 10);
        tr.querySelector('.txtPrix').innerText = prix + "$";
        tr.querySelector('.txtStatut').innerText = statut;

        tr.dataset.id = id;
        tableCommande.append(tr);
    }

    //Fonction pour chercher les commandes du client
    const getAllCommandeServeur = async () => {

        listeCommande = [];

        let response = await fetch('/commandeAll', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
                id: idCompte
            })
        });

        if(response.ok){
            listeCommande = await response.json();

            for(let commande of listeCommande){
                let responseStatutNom = await fetch('/statutNom', {
                    method: 'POST',
                    headers: { 'Content-Type': 'application/json' },
                    body: JSON.stringify({
                        id: commande.id_statut
                    })
                });

                let dataStatutNom = await responseStatutNom.json();

                ajouterCommandeRow(commande.id_commande, commande.date_commander, commande.prix_total, dataStatutNom[0].nom);
            }
            ActualiserTable();
        }
    }

    //Fonction pour chercher le compte sur la session
    const getCompteServeur = async () => {
        let response = await fetch('/compte');

        if(response.ok){
             let compte = await response.json();
 
             idCompte = compte.id_compte;
             getAllCommandeServeur();
        }
    }
    
    //Fonction pour effacer le compte de la session active
    const logoutServeur = async () => {
        let response = await fetch('/logout');
        
        if(response.ok){
            window.location.replace("/login.html")
        }
    }
    
    //Fonction pour mettre a jour la table de commande
    const ActualiserTable = () => {

        if(listeCommande.length != 0){
            txtTitre.innerText = "Tout les commandes";
            tableCommande.style.visibility = "visible";
            txtTitre.style.textAlign = "left";
        }
        else{
            txtTitre.innerText = "Aucune commande";
            tableCommande.style.visibility = "hidden";
            txtTitre.style.textAlign = "center";
        }
    }

    getCompteServeur();
    btnLogout.addEventListener('click', logoutServeur);
})();