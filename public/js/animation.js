
//Ajouter eventlistener pour retirer le loader apres que le site web est charger
window.addEventListener('load', () => {
    const preload = document.querySelector('.wrapper-loader');
    preload.classList.add("preload-finish");
});