(() => {

    let formInscription = document.getElementById("formInscription");
    let txtNom = document.getElementById("txtNom");
    let txtPrenom = document.getElementById("txtPrenom");
    let txtCourriel = document.getElementById("txtCourriel");
    let txtAdresse = document.getElementById("txtAdresse");
    let txtPassword = document.getElementById("txtPassword");
    let txtVerification = document.getElementById("txtVerification");
    
    //Fonction pour ajouter nouveau compte a la base de donnees(table compte)
    const ajouterCompteServeur = async (event) => {
        event.preventDefault();

        if(txtNom.value != "" && txtPrenom.value != "" && txtCourriel.value != "" && txtAdresse.value != "" && txtPassword.value != "" && txtVerification.value != ""){

            if(txtPassword.value == txtVerification.value){

                let responseTypeCompte = await fetch('/typeCompte', {
                    method: 'POST',
                    headers: { 'Content-Type': 'application/json' },
                    body: JSON.stringify({
                        nom: "Client"
                    })
                });

                let typeCompteClient = await responseTypeCompte.json();

                let response = await fetch('/inscription', {
                    method: 'POST',
                    headers: { 'Content-Type': 'application/json' },
                    body: JSON.stringify({
                        nom: txtNom.value,
                        prenom: txtPrenom.value,
                        adresse: txtAdresse.value,
                        courriel: txtCourriel.value,
                        password: txtPassword.value,
                        IdTypeCompte: typeCompteClient[0].id_type_compte
                    })
                });
        
                if(response.status == 200){
                    window.location.replace("./compte.html");
                }
                else{
                    alert("Une erreur est survenu.\nVeuillez recommancer votre inscription.");
                }
            }
            else{
                alert("Le mot de passe entrer et la vérification ne sont pas le même.\nVeuillez recommancer votre inscription.");
            }
        }
        else{
            alert("L'information remplit n'est pas valide ou manquante.\nVeuillez recommancer votre inscription.");
        }
    }

    formInscription.addEventListener('submit', ajouterCompteServeur);
    
})();