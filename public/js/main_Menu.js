(() => {
    let lstProduit = document.getElementById('lstProduitMenu');
    let templateProduit = document.getElementById('produit-template');

    let listeProduits = [];

    //Fonction pour ajouter produit a la liste du client
    const ajouterProduitUl = (id, titre, prix, image) => {
        let li = templateProduit.content.firstElementChild.cloneNode(true);
        li.querySelector('.txtTitre').innerText = titre;
        li.querySelector('.txtPrix').innerText = prix;
        li.querySelector('.imgProduit').src = "./img/" + image;
        li.querySelector('.btnProduit').addEventListener('click', ajouterPanierServeur);
        li.querySelector('.txtQuantite').id = "nb" + id;

        li.dataset.id = id;
        lstProduit.append(li);
    }

    //Fonction pour ajouter produit au panier du client
    const ajouterPanierServeur = (event) => {
        let idProduit = event.target.parentNode.parentNode.dataset.id;
        let titreProduit = "Test";
        let prixProduit = 1;
        let quantiteProduit = 1;
        let imageProduit = "Test";

        for(let produit of listeProduits){
            if(produit.id_produit == idProduit){
                titreProduit = produit.nom;
                prixProduit = produit.prix;
                quantiteProduit = document.getElementById('nb' + produit.id_produit).value;
                imageProduit = produit.image;
            }
        }
        
        fetch('/panier', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
                id: idProduit,
                titre: titreProduit,
                prix: prixProduit,
                quantite: quantiteProduit,
                image: imageProduit
            })
        });
    }

    //Fonction pour chercher tout les produits
    const getProduitServeur = async () => {
        let response = await fetch('/produit');

        if(response.ok){
            listeProduits = await response.json();

            for(let produit of listeProduits){
                ajouterProduitUl(produit.id_produit, produit.nom, produit.prix, produit.image);
            }
        }
    }

    getProduitServeur();
})();