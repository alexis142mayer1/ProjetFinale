(() => {
    let templateRow = document.getElementById('template-row');
    let tableCommande = document.getElementById("tableCommande");
    let txtTitre = document.getElementById("txtTitre");
    let btnLogout = document.getElementById("btnLogout");
    
    let idCompte = 0;
    let listeCommande = [];
    
    //Fonction pour ajouter row de la table commande pour le client
    const ajouterCommandeRow = (id, date, prix, statut) => {
        let tr = templateRow.content.firstElementChild.cloneNode(true);
        tr.querySelector('.txtDate').innerText = date.substring(0, 10);
        tr.querySelector('.txtPrix').innerText = prix + "$";
        tr.querySelector('.txtStatut').value = statut;
        tr.querySelector('.txtStatut').addEventListener('input', sauvegarderCommandeServeur);

        tr.dataset.id = id;
        tableCommande.append(tr);
    }

    //Fonction pour chercher les commandes de l'employer sur le serveur
    const getCommandeServeur = async () => {

        listeCommande = [];

        let response = await fetch('/commandeEmployer', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
                id: idCompte
            })
        });

        if(response.ok){
            listeCommande = await response.json();

            for(let commande of listeCommande){
                let responseStatutNom = await fetch('/statutNom', {
                    method: 'POST',
                    headers: { 'Content-Type': 'application/json' },
                    body: JSON.stringify({
                        id: commande.id_statut
                    })
                });

                let dataStatutNom = await responseStatutNom.json();

                ajouterCommandeRow(commande.id_commande, commande.date_commander, commande.prix_total, dataStatutNom[0].nom);
            }
            ActualiserTable();
        }
    }

    //Fonction pour chercher compte sur la session
    const getCompteServeur = async () => {
        let response = await fetch('/compte');

        if(response.ok){
             let compte = await response.json();
 
             idCompte = compte.id_compte;
             getCommandeServeur();
        }
    }
    
    //Fonction pour effacer le compte sur la session active
    const logoutServeur = async () => {
        let response = await fetch('/logout');
        
        if(response.ok){
            window.location.replace("/login.html")
        }
    }
    
    //Fonction pour modifier le statut de la commande de l'employer
    const sauvegarderCommandeServeur = async (event) => {

        let idCommande = event.target.parentNode.parentNode.dataset.id;
        let response = await fetch('/statut');
        let txtStatutTarget = event.target;

        if(response.ok){
            let listeStatut = await response.json();

            for(let statuts of listeStatut){
                if(statuts.nom == txtStatutTarget.value){
                    txtStatutTarget.style.borderColor = "green"

                    let responseIdStatut = await fetch('/statut', {
                        method: 'POST',
                        headers: { 'Content-Type': 'application/json' },
                        body: JSON.stringify({
                            nom: statuts.nom
                        })
                    });
                    
                    let idStatut = await responseIdStatut.json();

                    let responseUpdateStatut = await fetch('/commandeStatut', {
                        method: 'POST',
                        headers: { 'Content-Type': 'application/json' },
                        body: JSON.stringify({
                            id: idCommande,
                            idStatut: idStatut[0].id_statut
                        })
                    });

                    break;
                }
                else{
                    txtStatutTarget.style.borderColor = "red";
                }
            }
            ActualiserTable();
        }
    }
    
    //Fonction pour mettre a jour le table de commande
    const ActualiserTable = () => {

        if(listeCommande.length != 0){
            txtTitre.innerText = "Vos commandes";
            tableCommande.style.visibility = "visible";
            txtTitre.style.textAlign = "left";
        }
        else{
            txtTitre.innerText = "Aucune commande";
            tableCommande.style.visibility = "hidden";
            txtTitre.style.textAlign = "center";
        }
    }

    getCompteServeur();
    btnLogout.addEventListener('click', logoutServeur);
})();