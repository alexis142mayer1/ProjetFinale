(async () => {

    let txtUsername = document.getElementById("txtUsername");
    let txtPassword = document.getElementById("txtPassword");
    let formLogin = document.getElementById("formLogin");

    let responseLogin = await fetch('/identifier');

    //Condition pour verifier si l'utilisateur est deja identifier. Si oui, un client ou employer et le rediriger a la bonne page
    if(responseLogin.status == 200){
        let responseTypeCompte = await fetch('/typeCompte', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
                nom: "Employer"
            })
        });
        
        let typeCompte = await responseTypeCompte.json();
        let responseCompte = await fetch('/compte');
        let Compte = await responseCompte.json();
        window.location.replace("./compte.html");
        
        if(typeCompte[0].id_type_compte == Compte.id_type_compte){
            window.location.replace("./compteEmployer.html");
        }
        else{
            window.location.replace("./compte.html");
        }
    }
    
    //Fonction pour ajouter un compte a la session sont courriel et mot de passe
    const ajouterLogin = async (event) => {
        event.preventDefault();
        
        if(txtUsername.value != "" && txtPassword.value != ""){
            let response = await fetch('/login', {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify({
                    username: txtUsername.value,
                    password: txtPassword.value
                })
            });
    
            if (response.status == 200){
                
                let responseTypeCompte = await fetch('/typeCompte', {
                    method: 'POST',
                    headers: { 'Content-Type': 'application/json' },
                    body: JSON.stringify({
                        nom: "Employer"
                    })
                });
                
                let typeCompte = await responseTypeCompte.json();
                let responseCompte = await fetch('/compte');
                let Compte = await responseCompte.json();

                if(typeCompte[0].id_type_compte == Compte.id_type_compte){
                    window.location.replace("./compteEmployer.html");
                }
                else{
                    window.location.replace("./compte.html");
                }
            }
            else{
                alert("Votre compte ou mot de passe ne sont pas valide.");
            }
        }
    }

    formLogin.addEventListener('submit', ajouterLogin);
})();