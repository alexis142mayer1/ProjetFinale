(() => {
    
    let formInformation = document.getElementById("formInformation");
    let txtNom = document.getElementById("txtNom");
    let txtPrenom = document.getElementById("txtPrenom");
    let txtCourriel = document.getElementById("txtCourriel");
    let txtAdresse = document.getElementById("txtAdresse");
    let btnLogout = document.getElementById("btnLogout");

    let idCompte = 0;

    //Fonction pour chercher le compte sur la session
    const getCompteServeur = async () => {
        let response = await fetch('/compte');

        if(response.ok){
             let compte = await response.json();

            txtNom.value = compte.nom;
            txtPrenom.value = compte.prenom;
            txtCourriel.value = compte.courriel;
            txtAdresse.value = compte.adresse;
            idCompte = compte.id_compte;
        }
    }
    
    //Fonction pour mettre a jour l'information du compte de la session
    const updateCompteServeur = async (event) => {
        event.preventDefault();

        if(txtNom.value != "" && txtPrenom.value != "" && txtAdresse.value != "" && txtCourriel.value != ""){
            
            let response = await fetch('/updateCompte', {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify({
                    nom: txtNom.value,
                    prenom: txtPrenom.value,
                    adresse: txtAdresse.value,
                    courriel: txtCourriel.value,
                    id: idCompte
                })
            });

            if(response.status == 200){
                alert("Mise a jour completer!");
                window.location.replace("./compte.html");
            }
            else{
                alert("Erreur, veuillez recommancer l'insertion.");
            }
        }
        else{
            alert("Pour mettre à jour l'information de votre compte,\nvous devez remplir tout les informations.");
        }
    }
    
    //Fonction pour effacer le compte sur la session active
    const logoutServeur = async () => {
        let response = await fetch('/logout');
        
        if(response.ok){
            window.location.replace("/login.html")
        }
    }
    
    getCompteServeur();
    btnLogout.addEventListener('click', logoutServeur);
    formInformation.addEventListener('submit', updateCompteServeur);
})();