(() => {
    
    let formRevision = document.getElementById("formRevision");
    let txtNom = document.getElementById("txtNom");
    let txtPrenom = document.getElementById("txtPrenom");
    let txtCourriel = document.getElementById("txtCourriel");
    let txtAdresse = document.getElementById("txtAdresse");
    let txtFraisLivraison = document.getElementById("txtFraisLivraison");
    let txtSousTotal = document.getElementById("txtSousTotal");
    let txtTotal = document.getElementById("txtTotal");

    let listePanier = [];
    let idCompte = 0;
    let Totaux = 0;

    //Fonction pour chercher les produits du panier
    const getPanierServeur = async () => {
        listePanier = [];
        let response = await fetch('/panier');

        if(response.ok){
            listePanier = await response.json();

            for(let produit of listePanier){
                Totaux += parseFloat(produit.prix) * parseFloat(produit.quantite)
            }
            calculerMontant();
        }
    }

    //Fonction pour chercher le compte sur la session
    const getCompteServeur = async () => {
        let response = await fetch('/compte');

        if(response.ok){
             let compte = await response.json();

            txtNom.value = compte.nom;
            txtPrenom.value = compte.prenom;
            txtCourriel.value = compte.courriel;
            txtAdresse.value = compte.adresse;
            idCompte = compte.id_compte;
        }
    }

    //Fonction pour ajouter une commande dans la base de donnes(table commmande et commande_produit)
    const ajouterCommandeServeur = async (event) => {
        event.preventDefault();

        let Total = txtTotal.value.substring(0, 6);
        let dateNow = new Date();

        let responseStatut = await fetch('/statut', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
                nom: "En traitement" 
            })
        });

        let statutNom = await responseStatut.json();

        let response = await fetch('/commande', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
                id: idCompte,
                dateCommander: dateNow.getFullYear() + "-" + dateNow.getMonth() + "-" + dateNow.getDate(),
                total: parseFloat(Total).toFixed(2),
                idStatut: statutNom[0].id_statut
            })
        });
        
        let IdCommandeCreer = await response.json();

        for(let produit of listePanier){
             await fetch('/commandeProduit', {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify({
                    idCommande: IdCommandeCreer,
                    idProduit: parseInt(produit.id),
                    quantite: parseInt(produit.quantite)
                })
            });
        }

        if(response.status == 200){
            alert("Votre commande à été soumis!\nMerci de votre collaboration.");
            window.location.replace("./compte.html");
        }
        else{
            alert("Une erreur est survenu.\nVeuillez recommancer la commande.");
        }
    }

    //Fonction pour calculer le montant total de la commande
    const calculerMontant = () => {

        if(txtAdresse.value.indexOf(1)){
            txtFraisLivraison.value = 3.99;
        }
        else if(txtAdresse.value.indexOf(2)){
            txtFraisLivraison.value = 4.99;
        }
        else if(txtAdresse.value.indexOf(3)){
            txtFraisLivraison.value = 5.99;
        }
        else{
            txtFraisLivraison.value = 9.99;
        }

        txtSousTotal.value = Totaux.toFixed(2);
        txtTotal.value = (parseFloat(txtFraisLivraison.value) + parseFloat(Totaux)).toFixed(2);

        txtSousTotal.value += " $";
        txtTotal.value += " $";

        if(txtFraisLivraison.value.indexOf('$') == -1){
            txtFraisLivraison.value += " $";
        }
    }

    getPanierServeur();
    getCompteServeur();
    formRevision.addEventListener('submit', ajouterCommandeServeur);
})();