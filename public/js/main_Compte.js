(() => {

    let tableCommande = document.getElementById("tableCommande");
    let txtSalutation = document.getElementById("txtSalutation");
    let templateRow = document.getElementById('template-row');
    let txtTitre = document.getElementById("txtTitre");
    let btnLogout = document.getElementById("btnLogout");

    let idCompte = 0;
    let listeCommande = [];
    let IdStatut01 = 0;
    let IdStatut02 = 0;
    let IdStatut03 = 0;
    
    //Fonction pour ajouter table row d'une commande pour le client
    const ajouterCommandeRow = (id, date, prix, statut) => {
        let tr = templateRow.content.firstElementChild.cloneNode(true);
        tr.querySelector('.txtDate').innerText = date.substring(0, 10);
        tr.querySelector('.txtPrix').innerText = prix + "$";
        tr.querySelector('.txtStatut').innerText = statut;

        tr.dataset.id = id;
        tableCommande.append(tr);
    }

    //Fonction pour chercher les commandes avec un statut qui n'est pas 'Terminer' du client
    const getCommandeActiveServeur = async () => {

        listeCommande = [];

        let responseStatut01 = await fetch('/statut', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
                nom: "En traitement"
            })
        });
        
        let responseStatut02 = await fetch('/statut', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
                nom: "En cuisine"
            })
        });

        let responseStatut03 = await fetch('/statut', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
                nom: "En livraison"
            })
        });

        IdStatut01 = await responseStatut01.json();
        IdStatut02 = await responseStatut02.json();
        IdStatut03 = await responseStatut03.json();

        let response = await fetch('/commandeActive', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
                id: idCompte,
                id01: IdStatut01[0].id_statut,
                id02: IdStatut02[0].id_statut,
                id03: IdStatut03[0].id_statut
            })
        });

        if(response.ok){
            listeCommande = await response.json();

            for(let commande of listeCommande){
                let responseStatutNom = await fetch('/statutNom', {
                    method: 'POST',
                    headers: { 'Content-Type': 'application/json' },
                    body: JSON.stringify({
                        id: commande.id_statut
                    })
                });

                let dataStatutNom = await responseStatutNom.json();

                ajouterCommandeRow(commande.id_commande, commande.date_commander, commande.prix_total, dataStatutNom[0].nom);
            }
            ActualiserTable();
        }
    }

    //Fonction pour chercher le compte sur la session
    const getCompteServeur = async () => {
        let response = await fetch('/compte');

        if(response.ok){
             let compte = await response.json();
 
             txtSalutation.innerText = "Bienvenue " + compte.prenom + "!";
             idCompte = compte.id_compte;
        }
    }
    
    //Fonction pour effacer le compte sur la session active
    const logoutServeur = async () => {
        let response = await fetch('/logout');
        
        if(response.ok){
            window.location.replace("/login.html")
        }
    }

    //Fonction pour mettre a jour le tableau de commande
    const ActualiserTable = () => {

        if(listeCommande.length != 0){
            txtTitre.innerText = "Commande active";
            tableCommande.style.visibility = "visible";
            txtTitre.style.textAlign = "left";
        }
        else{
            txtTitre.innerText = "Aucune commande active";
            tableCommande.style.visibility = "hidden";
            txtTitre.style.textAlign = "center";
        }
    }

    getCompteServeur();
    getCommandeActiveServeur();
    btnLogout.addEventListener('click', logoutServeur);
})();