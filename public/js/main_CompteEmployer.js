(() => {

    let tableCommande = document.getElementById("tableCommande");
    let txtSalutation = document.getElementById("txtSalutation");
    let templateRow = document.getElementById('template-row');
    let txtTitre = document.getElementById("txtTitre");
    let btnLogout = document.getElementById("btnLogout");
    let tBody = document.getElementById("tBodyCompteEmployer");

    let idCompte = 0;
    let listeCommande = [];
    let IdStatut01 = 0;
    let IdStatut02 = 0;
    let IdStatut03 = 0;
    
    //Fonction pour ajouter table row d'une commande pour le client
    const ajouterCommandeRow = (id, date, prix, statut, idEmployer) => {
        let tr = templateRow.content.firstElementChild.cloneNode(true);
        tr.querySelector('.txtDate').innerText = date.substring(0, 10);
        tr.querySelector('.txtPrix').innerText = prix + "$";
        tr.querySelector('.txtStatut').innerText = statut;
        tr.querySelector('.btnAcquerir').addEventListener('click', acquerirCommandeServeur);

        if(idEmployer == null){
            tr.querySelector('.btnAcquerir').style.visibility = "visible";
        }
        else{
            tr.querySelector('.btnAcquerir').style.visibility = "hidden";
        }

        tr.dataset.id = id;
        tableCommande.append(tr);
    }

    //Fonction pour chercher tout les commandes sur la base de donnes(table commande)
    const getAllCommandeServeur = async () => {

        listeCommande = [];

        let response = await fetch('/commandeAllEmployer');

        if(response.ok){
            listeCommande = await response.json();

            for(let commande of listeCommande){
                let responseStatutNom = await fetch('/statutNom', {
                    method: 'POST',
                    headers: { 'Content-Type': 'application/json' },
                    body: JSON.stringify({
                        id: commande.id_statut
                    })
                });

                let dataStatutNom = await responseStatutNom.json();

                ajouterCommandeRow(commande.id_commande, commande.date_commander, commande.prix_total, dataStatutNom[0].nom, commande.id_employer);
            }
            ActualiserTable();
        }
    }

    //Fonction pour chercher le compte sur la session
    const getCompteServeur = async () => {
        let response = await fetch('/compte');

        if(response.ok){
             let compte = await response.json();
 
             txtSalutation.innerText = "Bienvenue " + compte.prenom + "!";
             idCompte = compte.id_compte;
        }
    }
    
    //Fonction pour effacer le compte sur la session active
    const logoutServeur = async () => {
        let response = await fetch('/logout');
        
        if(response.ok){
            window.location.replace("/login.html")
        }
    }
    
    //Fonction pour ajouter un id_employer sur une commande qui n'en contient pas
    const acquerirCommandeServeur = async (event) => {

        let response = await fetch('/acquerirCommandeEmployer', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
                idEmployer: idCompte,
                idCommande: event.target.parentNode.parentNode.dataset.id
            })
        });

        viderTableCommande();
        getAllCommandeServeur();
    }

    //Fonction pour vider le tableau du client
    const viderTableCommande = () => {
        tableCommande.innerHTML = "";
    }

    //Fonction pour mettre a jour le tableau du client
    const ActualiserTable = () => {
        if(listeCommande.length != 0){
            txtTitre.innerText = "Tout les commandes";
            tableCommande.style.visibility = "visible";
            txtTitre.style.textAlign = "left";
        }
        else{
            txtTitre.innerText = "Aucune commande";
            tableCommande.style.visibility = "hidden";
            txtTitre.style.textAlign = "center";
        }
    }

    getCompteServeur();
    getAllCommandeServeur();
    btnLogout.addEventListener('click', logoutServeur);
})();